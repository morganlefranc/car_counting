import argparse
import os

import cv2
import numpy as np


def main():
    gt_dir = "/HDD/cowc/datasets/ground_truth_sets/"
    dir_names = ['Potsdam_ISPRS', 'Selwyn_LINZ', 'Toronto_ISPRS', 'Utah_AGRC']
    downsample_ratio = 2
    label_signature = "_Annotated_Cars.png"

    for dir_name in dir_names:
        print("Processing {}".format(dir_name))
        in_dir = os.path.join(gt_dir, dir_name)
        out_dir = os.path.join(gt_dir, dir_name + '_' + str(15 * downsample_ratio)) # Original images are 15cm/pix
        os.makedirs(out_dir, exist_ok=True)

        in_labels_paths = [os.path.join(in_dir, f) for f in os.listdir(in_dir) if f.endswith(label_signature)]
        in_imgs_paths = [f.replace(label_signature, '.png') for f in in_labels_paths]
        out_labels_paths = [os.path.join(out_dir, os.path.basename(f)) for f in in_labels_paths]
        out_imgs_paths = [os.path.join(out_dir, os.path.basename(f)) for f in in_imgs_paths]

        for i, in_img_path in enumerate(in_imgs_paths):
            in_img = cv2.imread(in_img_path)
            h, w = in_img.shape[:2]
            out_shape = (w // downsample_ratio, h // downsample_ratio)
            out_img = cv2.resize(in_img, out_shape, cv2.INTER_LINEAR)
            cv2.imwrite(out_imgs_paths[i], out_img)

        for i, in_labels_path in enumerate(in_labels_paths):
            in_label = cv2.imread(in_labels_path)
            h, w = in_label.shape[:2]
            out_shape = (w // downsample_ratio, h // downsample_ratio)
            out_label = cv2.resize(in_label, out_shape, cv2.INTER_MAX)
            out_label[out_label[:, :, 2] != 0] = (0, 0, 255)
            cv2.imwrite(out_labels_paths[i], out_label)



if __name__ == "__main__":
    main()
